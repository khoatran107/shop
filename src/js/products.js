const productsListDisplay = document.querySelector('.products ul');
const totalQuantityDisplay = document.getElementById('total-quantity');
const dishesQuantity = (document.cookie === '') ? {} : JSON.parse(document.cookie);

const productsJSON = [
  {
    image: './src/images/dishes/banh_mi.jpeg',
    name: 'Banh Mi',
    price: 1.0,
  },
  {
    image: './src/images/dishes/bubble_tea.jpg',
    name: 'Bubble tea',
    price: 2.0,
  },
  {
    image: './src/images/dishes/chicken.jpg',
    name: 'Chicken',
    price: 4.0,
  },
  {
    image: './src/images/dishes/fried_chicken.jpg',
    name: 'Fried chicken',
    price: 4.0,
  },
  {
    image: './src/images/dishes/iced_drink.jpeg',
    name: 'Iced drink',
    price: 2.0,
  },
  {
    image: './src/images/dishes/meat.jpeg',
    name: 'Meat',
    price: 5.0,
  },
  {
    image: './src/images/dishes/pasta.jpeg',
    name: 'Pasta',
    price: 3.0,
  },
  {
    image: './src/images/dishes/pizza.jpg',
    name: 'Pizza',
    price: 4.0,
  },
  {
    image: './src/images/dishes/snack.jpeg',
    name: 'Snack',
    price: 1.0,
  },
];
