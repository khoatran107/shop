
function reloadTotalQuantity() {
  let totalQuantity = 0;
  for (key in dishesQuantity) totalQuantity += dishesQuantity[key];
  totalQuantityDisplay.textContent = totalQuantity;
  totalQuantityDisplay.style.display = totalQuantity > 0 ? 'inline' : 'none';
}

function switchButtons(currentBtn) {
  const children = currentBtn.parentNode.parentNode.children;
  children[children.length - 1].style.display = dishesQuantity.hasOwnProperty(
    currentBtn.id
  )
    ? 'flex'
    : 'none';
  children[children.length - 2].style.display = !dishesQuantity.hasOwnProperty(
    currentBtn.id
  )
    ? 'flex'
    : 'none';
  children[children.length - 1].children[1].textContent =
    dishesQuantity[currentBtn.id];
}

function saveData() {
  document.cookie = JSON.stringify(dishesQuantity);
}

window.onload = function () {
  let curId = 0;
  productsJSON.forEach((dish) => {
    const dishHTML = `
        <li>
          <figure>
            <img src="${dish.image}" alt="${dish.name}">
          </figure>
          <div class="info">
            <div class="product-name"><h1>${dish.name}</h1></div>
            <div class="price">${dish.price.toFixed(2)}</div>
          </div>
          <div class="edit" style="display:${
            !dishesQuantity.hasOwnProperty(curId) ? 'flex' : 'none'
          };">
            <button class="add-btn" id="${curId}">
              ADD TO CART
            </button>
          </div>
            <div class="edit-hidden" style="display:${
              dishesQuantity.hasOwnProperty(curId) ? 'flex' : 'none'
            };">
            <button class="subtract-btn" id="${curId}">-</button>
            <span class="quantity">${dishesQuantity[curId]}</span>
            <button class="add-btn" id="${curId}">+</button>
          </div>
        </li>
      `;
    productsListDisplay.innerHTML += dishHTML;
    curId++;
  });

  reloadTotalQuantity();

  const subtractBtns = Array.from(
    document.getElementsByClassName('subtract-btn')
  );

  subtractBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
      const curId = btn.id;
      if (dishesQuantity.hasOwnProperty(curId)) {
        dishesQuantity[curId]--;
        if (dishesQuantity[curId] === 0) {
          delete dishesQuantity[curId];
        }
      }
      reloadTotalQuantity();
      switchButtons(btn);
      saveData();
    });
  });

  const addBtns = Array.from(document.getElementsByClassName('add-btn'));

  addBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
      if (dishesQuantity.hasOwnProperty(btn.id)) {
        dishesQuantity[btn.id]++;
      } else dishesQuantity[btn.id] = 1;
      reloadTotalQuantity();
      switchButtons(btn);
      saveData();
    });
  });
};
