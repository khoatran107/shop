const totalPriceDisplay = document.getElementById('total-price');

function reloadTotalQuantity() {
  let totalQuantity = 0;
  for (key in dishesQuantity) totalQuantity += dishesQuantity[key];
  totalQuantityDisplay.textContent = totalQuantity;
  totalQuantityDisplay.style.display = totalQuantity > 0 ? 'inline' : 'none';
}

function reloadTotalPrice() {
  let totalPrice = 0;
  for (key in dishesQuantity) {
    totalPrice += dishesQuantity[key] * productsJSON[key].price;
  }
  totalPriceDisplay.textContent = totalPrice.toFixed(2);
}

function reloadCurrentPrice(btn) {
  const curId = btn.id;
  btn.parentNode.children[1].textContent = dishesQuantity[curId];
  btn.parentNode.parentNode.children[3].textContent = (
    dishesQuantity[curId] * productsJSON[curId].price
  ).toFixed(2);
}

function saveData() {
  document.cookie = JSON.stringify(dishesQuantity);
}

window.onload = () => {
  for (id in dishesQuantity) {
    const dish = productsJSON[id];
    const dishHTML = `
        <li>
          <figure>
            <img src="${dish.image}" alt="${dish.name}">
          </figure>
          <div class="info">
            <div class="product-name"><h1>${dish.name}</h1></div>
            <div class="price">${dish.price.toFixed(2)}</div>
          </div>
          <div class="edit">
            <button class="subtract-btn" id="${id}">-</button>
            <span class="quantity">${dishesQuantity[id]}</span>
            <button class="add-btn" id="${id}">+</button>
          </div>
          <div class="total">${(dish.price * dishesQuantity[id]).toFixed(
            2
          )}</div>
        </li>
      `;
    productsListDisplay.innerHTML += dishHTML;
  }
  reloadTotalQuantity();
  reloadTotalPrice();

  const subtractBtns = Array.from(
    document.getElementsByClassName('subtract-btn')
  );

  subtractBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
      const curId = btn.id;
      if (dishesQuantity.hasOwnProperty(curId)) {
        dishesQuantity[curId]--;
      }
      if (dishesQuantity[curId] === 0) {
        delete dishesQuantity[curId];
        btn.parentNode.parentNode.parentNode.removeChild(
          btn.parentNode.parentNode
        );
      }
      reloadCurrentPrice(btn);
      reloadTotalPrice();
      reloadTotalQuantity();
      saveData();
    });
  });

  const addBtns = Array.from(document.getElementsByClassName('add-btn'));

  addBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
      const curId = btn.id;
      if (dishesQuantity.hasOwnProperty(curId)) {
        dishesQuantity[curId]++;
      }
      reloadCurrentPrice(btn);
      reloadTotalPrice();
      reloadTotalQuantity();
      saveData();
    });
  });

  document.getElementById('clear').addEventListener('click', () => {
    productsListDisplay.innerHTML = '';
    for (key in dishesQuantity) delete dishesQuantity[key];
    totalQuantity = 0;
    reloadTotalPrice();
    reloadTotalQuantity();
    saveData();
  });

  const checkoutBtn = document.getElementById('checkout');
  const popupBackgroud = document.querySelector('.popup-background');
  const popup = document.querySelector('.popup');
  const form = document.querySelector('form');
  const finish = document.querySelector('.finish');

  checkoutBtn.addEventListener('click', () => {
    popupBackgroud.style.display = 'flex';
    form.style.display = 'block';
    finish.style.display = 'none';
  });

  popupBackgroud.addEventListener('click', () => {
    popupBackgroud.style.display = 'none';
  });

  popup.addEventListener('click', (event) => {
    event.stopPropagation();
  });

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    productsListDisplay.innerHTML = '';
    for (key in dishesQuantity) delete dishesQuantity[key];
    totalQuantity = 0;
    reloadTotalPrice();
    reloadTotalQuantity();
    saveData();
    form.style.display = 'none';
    finish.style.display = 'flex';
    setTimeout(() => {
      window.location.replace('./index.html');
    }, 1000);

  });
};
